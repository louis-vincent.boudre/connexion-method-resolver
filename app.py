#!/usr/bin/env python3
import connexion
import datetime
import logging
import util

from connexion import NoContent
from connexion.resolver import Resolver
from abc import ABC, abstractmethod

# our memory-only pet storage

class PetController:
    def __init__(self):
        self.PETS = {}

    def get_pets(self, limit, animal_type=None):
        return {"self.PETS": [pet for pet in self.PETS.values() if not animal_type or pet['animal_type'] == animal_type][:limit]}

    def get_pet(self, pet_id):
        pet = self.PETS.get(pet_id)
        return pet or ('Not found', 404)

    def put_pet(self, pet_id, pet):
        exists = pet_id in self.PETS
        pet['id'] = pet_id
        if exists:
            logging.info('Updating pet %s..', pet_id)
            self.PETS[pet_id].update(pet)
        else:
            logging.info('Creating pet %s..', pet_id)
            pet['created'] = datetime.datetime.utcnow()
            self.PETS[pet_id] = pet
        return NoContent, (200 if exists else 201)

    def delete_pet(self, pet_id):
        if pet_id in self.PETS:
            logging.info('Deleting pet %s..', pet_id)
            del self.PETS[pet_id]
            return NoContent, 204
        else:
            return NoContent, 404

class AbstractControllerContainer(ABC):

    class ControllerResolver(Resolver):
        
        def __init__(self, container, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.container = container 

        def resolve_operation_id(self, operation):
            name_dot_action = self.container.controller_by_name(operation.operation_id)
            keyname, method = tuple(name_dot_action.split('.'))
            controller = self.container.controller_by_name(keyname)
            if not hasattr(controller, method):
                raise AttributeError("No method named `%s` for controller `%s`"%(method, keyname))
            return getattr(controller, method)
    
    @abstractmethod
    def add_controller(self, controller, keyname=None)
        """
        Adds a controller in the container.
        Parameters:
            - controller (object): the controller to add
            - keyname (string|None): the unique name of the controller. 
            If `keyname` is set, it overrides any predefined conventions
            enforced by current container.
        Returns:
            - string: the unique name of the controller.
        Raises:
            - ControllerKeyNameAlreadyExist: if the controller unique name is 
            already assigned to the same or another controller.
        """
        pass

    def add_controllers(self, controllers):
        """
        Adds multiple controllers.
        Params:
            - controllers (iterable): Controllers to add.
        Returns:
            - list[string]: unique names of the controllers.
        """
        return map(self.add_controller, controllers)
    
    def controller_by_name(self, controller_name, default=None):
        return default

    @property
    def connexion_resolver(self):
        return ControllerResolver(self)

class ControllerKeyNameAlreadyExist(Exception):
    pass

class ControllerContainer(AbstractControllerContainer):
    
    def __init__(self, *controllers, convention_name_mapper=utils.default_controller_name_convention)
        """
        Initializes a new instance of `ControllerContainer`
        Params:
            - controllers (*iterable): one or many controller to add after
            initialization.
            - convention_name_mapper (Function): function that maps
            a convention name over a controller class name.
            By default, every controller named `<Something>Controller` will be
            map to `<Something>`, eg : 
                - `HomeController` -> `Home`;
                - `ProductController`-> `Controller`.
        """
        super().__init__()
        # A map from convention name to actual controller instance.
        self.name2controller = {}
        self.convention_name_mapper = convention_name_mapper
        add_controllers(controllers)

    def add_controller(controller, keyname=None):
        keyname = keyname or self.convention_name_mapper(controller)
        if keyname in self.name2controller:
            raise ControllerKeyNameAlreadyExist("A controller has already been registerd as `%s`"%(keyname))
        self.name2controller[keyname] = controller
        return keyname

    def controller_by_name(self, controller_name, default=None):
        return self.name2controller.get(controller_name, default)

ControllerContainer(PetController())

logging.basicConfig(level=logging.INFO)

app = connexion.App(__name__, resolver=MyResolver())
app.add_api('swagger.yaml')
# set the WSGI application callable to allow using uWSGI:
# uwsgi --http :8080 -w app
application = app.app

if __name__ == '__main__':
    # run our standalone gevent server
    app.run(port=8080, server='gevent')
